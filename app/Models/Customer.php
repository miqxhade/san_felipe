<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Customer extends Model 
{
    use HasFactory;

        protected $table = 'customer';
     protected $guarded = ['id'];
 
   
 
     protected $dates = [
         'created_at',
         'updated_at',
     ];

     public function getBatch()
     {
        return $this->belongsToMany('App\Models\batch'); 
     }

     public function getOrder()
     {
        return $this->belongsToMany('App\Models\Order'); 
     }

}
