<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends Model
{
    use HasFactory;

    
    protected $table = 'product';
    protected $guarded = ['id'];

  

    protected $dates = [
        'created_at',
        'updated_at',
    ];



    public function user()
    {
        return $this->hasMany(User::class); 
    }

}
