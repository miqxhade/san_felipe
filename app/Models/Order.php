<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Order extends Model 
{
    use HasFactory;


    protected $table = 'order_list';
    protected $guarded = ['id'];
    

  

    protected $dates = [
        'created_at',
        'updated_at',
    ];


    public function Product()
    {
        return $this->hasMany(Product::class)->orderBy('id', 'desc');
    }
    public function getCustomerOrderName()
    {
        return $this->belongsTo(Customer::class);
    }
}
