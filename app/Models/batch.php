<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class batch extends Model
{
    use HasFactory;

    protected $table = 'batch_list';
    protected $guarded = ['id'];

  

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function getNameCustomer()
    {
        return $this->belongsToMany('App\Models\Customer'); 
    }
}
