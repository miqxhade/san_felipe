<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produce extends Model
{
    use HasFactory;
    protected $table = 'produce';
    protected $guarded = ['id'];
    

    public function getBatch()
    {
       return $this->belongsToMany('App\Models\batch'); 
    }

    public function Product()
    {
        return $this->hasMany(Product::class)->orderBy('id', 'desc');
    }
    public function getOrder()
    {
       return $this->belongsToMany('App\Models\Order'); 
    }
    protected $dates = [
        'created_at',
        'updated_at',
    ];

 
}
