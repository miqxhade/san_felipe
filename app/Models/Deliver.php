<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deliver extends Model
{
    use HasFactory;
    protected $table = 'deliver';
    protected $guarded = ['id'];
    

    public function getBatch()
    {
       return $this->belongsToMany('App\Models\batch'); 
    }

    public function getOrder()
    {
       return $this->belongsToMany('App\Models\Order'); 
    }

    public function Product()
    {
        return $this->hasMany(Product::class)->orderBy('id', 'desc');
    }
    protected $dates = [
        'created_at',
        'updated_at',
    ];

}
