<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use App\Notifications\DestroyProduct;
use App\Notifications\EditProduct;
use App\Notifications\TaskCompleted;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $products = Product::get();



        return view('product.index', compact('user', 'products'));
    }

    public function update(Request $request, $id)
    {

        try {
            //- update is_email_verified column
            $product = Product::find($id);
            $product->product_code = $request->product_code;
            $product->description = $request->description;
            $product->created_by = $request->created_by;
            $product->units = $request->units;

          $product->save();

          $admins = User::where('type','=','Admin')->get();
            Notification::send($admins,new EditProduct($product));
            return back()->with('success', trans('Product successfully updated'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {
            $destroy =  Product::find($id);
            Product::find($id)->delete();
            $admins = User::where('type','=','Admin')->get();

            Notification::send($admins,new DestroyProduct($destroy));
            return back()->with('success', trans('Product successfully deleted'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }



    public function store(Request $request)
    {

        $data = [
            'product_code'             => $request->product_code,
            'description'              => $request->description,
            'units'        => $request->units,
            'created_by'         => $request->created_by,

        ];
        $admins = User::where('type','=','Admin')->get();
 
        $user_create = Product::create($data);

        Notification::send($admins, new TaskCompleted($user_create));
        return back()->with('success', trans('Product successfully Added'));
    }
}
