<?php

namespace App\Http\Controllers;

use App\Models\batch as ModelsBatch;
use App\Models\Customer;
use App\Models\Deliver;
use App\Models\Order;
use App\Models\Produce;
use App\Models\Product;
use App\Models\User;
use App\Notifications\AcceptedOrder;
use App\Notifications\aOrdrer;
use App\Notifications\CreateBatch;
use App\Notifications\CreatedOrder;
use App\Notifications\DeleteOrder;
use App\Notifications\EditedOrder;
use App\Notifications\rejectOrder;
use App\Notifications\rOrdrer;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Auth\Access\Response;
use Illuminate\Bus\Batch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request as FacadesRequest;
use OrderListTable;

class OrderController extends Controller
{

    public function index()
    {
        $now=Carbon::now()->format('Y-m-d');
        $tom=Carbon::tomorrow()->format('Y-m-d');
        $future=Carbon::tomorrow()->addDay(1)->format('Y-m-d');
        $user = Auth::user();
        $pendings = ModelsBatch::where('status', '=', '0')->orderBy('id', 'desc')->paginate(10);
        $onproduction = ModelsBatch::where('status', '=', 'approve')->orderBy('id', 'desc')->paginate(10);
        $completed = ModelsBatch::where([['status', '!=', 'approve'],['status', '!=', '0']])->orderBy('id', 'desc')->paginate(10);
        return view('Order.index', compact('user', 'pendings', 'completed', 'onproduction','future','now','tom'));
    }



    public function create()
    {

        $batchid = ModelsBatch::latest()->first()->id;
        $batchid++;
        if (mb_strlen($batchid) == 1) {
            $zero_string = '00';
        } elseif (mb_strlen($batchid) == 2) {
            $zero_string = '0';
        } else {
            $zero_string = '';
        }
        $new_id = 'JO' . $zero_string . $batchid;

        $user = Auth::user();
        $customers = Customer::get();
        $users = User::where('type', '=', 'Product')->get();
        $products = Product::get();

    
        return view('Order.create', compact('user', 'customers', 'users', 'products', 'new_id'));
    }

    public function store(Request $request)
    {

        $data = [
            'batch_code'             => $request->new_ids,
            'order_by'              => $request->customer,
            'user_id'              => $request->user,
            'purchase_order'       => $request->purchase_order,
            'date_need'        => $request->date_needed,
            'status'        => '0',
            'note'        => $request->notes,
            'created_by'         => $request->created_by
        ];
        $batch = ModelsBatch::create($data);

        $input = $request->all();

  
        for ($i = 0; $i <= count($input); $i++) {
            if (empty($input['new_id'][$i])) {
                continue;
            }   

            $datas[] = [
                "product_code" =>  $input['product_codes'][$i],
                "batch_code" => $input['new_id'][$i],
                "bond" => $input['units'][$i],
                "quantity" => $input['quantity'][$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        $sampel = Order::insert($datas);
        $admins = User::where('type','=','Admin')->get();
        Notification::send($admins ,new CreateBatch($batch));
        return back()->with('success', trans('Product successfully Added'));
    }

    public function edit($id)
    {
        $batch_id = ModelsBatch::where('id', '=', $id)->get()->first();

        $batch_code = $batch_id->batch_code;
   
        $order_id = Order::where('batch_code', '=', $batch_code)->paginate(10);
      
        $company = Customer::where('customer_name', '=', $batch_id->order_by)->get()->first();

        return view('Order.edit', compact('batch_id', 'company', 'order_id','batch_code'));

    }

public function update(Request $request, $id){

  
    try {
        //- update is_email_verified column
        $order = Order::find($id);
        $order->quantity = $request->quantity;
        $order->created_by = $request->created_by;
        Notification::send(request()->user(),new EditedOrder($order));
        $order->save();
       
    
        return back()->with('success', trans('Job Order successfully updated'));
    } catch (Exception $e) {
        return back()->with('error', $e->getMessage());
    }

   
}

    public function application($id)
    {

        $batch_id = ModelsBatch::where('id', '=', $id)->get()->first();

    

        $batch_code = $batch_id->batch_code;

 

        $order_id = Order::where('batch_code', '=', $batch_code)->paginate(10);


        $company = Customer::where('customer_name', '=', $batch_id->order_by)->get()->first();

        return view('Order.open', compact('batch_id', 'company', 'order_id'));
    }

    public function production($id)
    {
      
        $batch_id = ModelsBatch::where('id', '=', $id)->get()->first();

        $batch_code = $batch_id->batch_code;
   
        $order_id = Order::where('batch_code', '=', $batch_code)->paginate(10);

        $company = Customer::where('customer_name', '=', $batch_id->order_by)->get()->first();

   
        return view('Order.production', compact('batch_id', 'company', 'order_id','batch_code'));
    }

    public function accept(Request $request, $id)
    {

        try {
            $order = ModelsBatch::find($id);
            $order->status = 'approve';
            $order->accepted_by = $request->created_by;
             $order->update();
             $admins = User::where('type','=','Admin')->get();

            Notification::send($admins,new aOrdrer($order));
            return redirect('order')->with('success', trans('Order has been approved'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function reject(Request $request, $id)
    {

        try {
            $order = ModelsBatch::find($id);
            $order->status = 'rejected';
            $order->rejected_by = $request->created_by;
             $order->save();
             $admins = User::where('type','=','Admin')->get();
            Notification::send($admins,new rOrdrer($order));
            return back('')->with('success', trans('Order has been rejected'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $order = Order::find($id);
            ModelsBatch::find($id)->delete();

            Notification::send(request()->user(),new DeleteOrder($order));
            return back()->with('success', trans('order successfully deleted'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
