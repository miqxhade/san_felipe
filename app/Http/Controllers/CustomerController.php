<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\User;
use App\Notifications\CreateCustomer;
use App\Notifications\DestryCustomer;
use App\Notifications\EditCustomer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class CustomerController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $customers = Customer::get();



        return view('customer.index', compact('user', 'customers'));
    }

    public function create()
    {
    }

    public function update(Request $request, $id)
    {

        try {
            //- update is_email_verified column
            $customer = Customer::find($id);
            $customer->customer_name = $request->customer_name;
            $customer->contact_no = $request->contact_no;
            $customer->contact_person = $request->contact_person;
            $customer->address = $request->address;
            $customer->remarks = $request->remarks;
            $customer->save();
            $admins = User::where('type','=','Admin')->get();

            Notification::send($admins,new EditCustomer($customer));
            return back()->with('success', trans('Customer successfully updated'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }
    public function destroy($id)
    {
        try {
            $customer = Customer::find($id);
           Customer::find($id)->delete();

            Notification::send(request()->user(),new DestryCustomer($customer));
            return back()->with('success', trans('Customer successfully deleted'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }



    public function store(Request $request)
    {
        $data = [
            'customer_name'             => $request->company_name,
            'contact_person'              => $request->contact_person,
            'contact_no'        => $request->contact_number,
            'address'         => $request->address,
            'assign'         => 'as',
            'remarks'         => $request->remarks,
            'created_by'         => $request->created_by,
        ];

        $customer = Customer::create($data);
        Notification::send(request()->user(),new CreateCustomer($customer));
        return back()->with('success', trans('user posted successfully'));
    }
}
