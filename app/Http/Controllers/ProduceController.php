<?php

namespace App\Http\Controllers;

use App\Models\batch;
use App\Models\Produce;
use App\Models\Product;
use App\Models\User;
use App\Notifications\CompletedOrder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ProduceController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $products = Product::paginate(10);



        return view('product.index', compact('user', 'products'));
    }

    public function update(Request $request)
    {
        try {
            $produced = Produce::where('item_id', '=', $request->item_id)->first();
            $sum = $produced->produced - $request->produced;
            $produced->produced = $sum;
            $produced->update();

            return back()->with('success', trans('Product successfully updated'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function complete(Request $request, $id)
    {
        try {
            $order =batch::find($id);
            $order->status = 'completed';
           $order->update();
           $admins = User::where('type','=','Admin')->get();
            Notification::send($admins,new CompletedOrder($order));
            return redirect('order')->with('success', trans('Order has been approved'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }



    public function store(Request $request)
    {
        try {
            $produced = Produce::where('item_id', '=', $request->item_id)->first();
            if ($produced == null) {
                $data = [
                    'item_id'             => $request->item_id,
                    'produced'              => $request->produced,
                ];
                Produce::create($data);
            } else {
                $sum = $produced->produced + $request->produced;
                $produced->produced = $sum;
                $produced->update();
            }
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return back()->with('success', trans('Produce successfully Added'));
    }
}
