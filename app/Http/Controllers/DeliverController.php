<?php

namespace App\Http\Controllers;

use App\Models\Deliver;
use Exception;
use Illuminate\Http\Request;

class DeliverController extends Controller
{

    public function store(Request $request)
    {
        try {
            $data = [
                'item_id'             => $request->item_id,
                'delivered'              => $request->delivered,
            ];
            Deliver::create($data);
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return back()->with('success', trans('Delivered successfully Added'));
    }

}
