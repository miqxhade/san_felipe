<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {


        $user = Auth::user();
        $user_id = $user->type;


        if ($user_id == "Admin") {
            return view('admin.dashboard',compact('user','user_id'));
        } elseif ($user_id == "Encoder") {
            return view('encoder.dashboard',compact('user','user_id'));
        } elseif ($user_id == "Product") {
            return view('production.dashboard',compact('user','user_id'));
        }
        else{

        }
    }
}
