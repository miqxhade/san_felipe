<?php

namespace App\Http\Controllers;

use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Models\Audit;

class LogController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $audits = Audit::paginate(10);



        return view('audit.index', compact('user', 'audits'));
    }
}
