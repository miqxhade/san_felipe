<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserNotificationController extends Controller
{
    public function show()
    {

        $notifications = tap(auth()->user()->unreadNotifications)->markAsRead();

        return view('audit.index',[
            'notifications' => $notifications
        ]);
    }
    
}
