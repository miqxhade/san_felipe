<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $users = User::paginate(10);

        return view('admin.settings', compact('user','users'));
    }

    public function destroy($id)
    {
        try {
            User::find($id)->delete();
            return back()->with('success', trans('User successfully deleted'));
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {

        $data = [
            'firstname'             => $request->first_name,
            'lastname'              => $request->last_name,
            'username'        => $request->username,
            'location'        => $request->location,
            'password'         =>  Hash::make ($request->password),
            'type'         => $request->type,
        ];

       
        $user_create = User::create($data);

        return redirect()->back()->with('success', trans('user posted successfully'));
    }
}
