<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Dashboard Route

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('/dashboard');

Route::get('logout', 'Auth\LoginController@getLogout');

//Order Rouote
Route::get('order', [App\Http\Controllers\OrderController::class, 'index'])->name('order');
Route::get('order.create', [App\Http\Controllers\OrderController::class, 'create'])->name('order.create');
Route::post('order.add',  [App\Http\Controllers\OrderController::class, 'store'])->name('order.add');
Route::get('order.update{id}',  [App\Http\Controllers\OrderController::class, 'application'])->name('order.update');
Route::get('production.update{id}',  [App\Http\Controllers\OrderController::class, 'production'])->name('production.update');
Route::get('order.edit{id}',  [App\Http\Controllers\OrderController::class, 'edit'])->name('order.edit');
Route::post('accept.update{id}',  [App\Http\Controllers\OrderController::class, 'accept'])->name('accept.update');
Route::post('reject.update{id}',  [App\Http\Controllers\OrderController::class, 'reject'])->name('reject.update');
Route::post('order.update{id}',  [App\Http\Controllers\OrderController::class, 'update'])->name('order.update{id}');
Route::delete('pending/{id}',  [App\Http\Controllers\OrderController::class, 'destroy'])->name('pending');

//Customer Route
Route::get('customer', [App\Http\Controllers\CustomerController::class, 'index'])->name('customer');
Route::post('customer.add',  [App\Http\Controllers\CustomerController::class, 'store'])->name('customer.add');
Route::post('customer.update{id}',  [App\Http\Controllers\CustomerController::class, 'update'])->name('customer.update{id}');
Route::delete('destroy_customer/{id}',  [App\Http\Controllers\CustomerController::class, 'destroy'])->name('destroy_customer');

//Product Route
Route::get('product', [App\Http\Controllers\ProductController::class, 'index'])->name('product');
Route::post('product.add',  [App\Http\Controllers\ProductController::class, 'store'])->name('product.add');
Route::post('product.update{id}',  [App\Http\Controllers\ProductController::class, 'update'])->name('product.update{id}');
Route::delete('destroy_product/{id}',  [App\Http\Controllers\ProductController::class, 'destroy'])->name('destroy_product');

//Complete Route
Route::post('complete.update{id}',  [App\Http\Controllers\ProduceController::class, 'complete'])->name('complete.update');

//Product Route
Route::post('produce.update',  [App\Http\Controllers\ProduceController::class, 'update'])->name('produce.update');
Route::post('produce.add',  [App\Http\Controllers\ProduceController::class, 'store'])->name('produce.add');

//Product Route
Route::post('deliver.add',  [App\Http\Controllers\DeliverController::class, 'store'])->name('deliver.add');

//Settings Route
Route::get('settings', [App\Http\Controllers\SettingsController::class, 'index'])->name('settings');
Route::delete('destroy_user/{id}',  [App\Http\Controllers\SettingsController::class, 'destroy'])->name('destroy_user');
Route::post('users.add',  [App\Http\Controllers\SettingsController::class, 'store'])->name('users.add');

//Logs Route
Route::get('log', [App\Http\Controllers\LogController::class, 'index'])->name('log');

Route::get('notification', [App\Http\Controllers\UserNotificationController::class, 'show'])->name('notification');