<div id="comeplete{{ $batch_code}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">Approve Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form method="post" action="{{ route('complete.update',$batch_id->id) }}" enctype="multipart/form-data">
            @csrf
              @method('POST')
              <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                <div class="modal-body text-center">
                    <p class="font-weight-semibold font-size-lg">Are you sure you want to complete {{ $batch_id->batch_code }} ? </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
                    <input class="btn btn-primary " type="submit" value="Yes">
            </form>
        </div>
    </div>
</div>
</div>