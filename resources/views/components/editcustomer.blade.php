 <!-- Vertical form modal -->
 <div id="editcustomer{{$customer->id}}" class="modal fade" tabindex="-1">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header  bg-slate-800">
                 <h5 class="modal-title">Edit {{$customer->customer_name}}</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>

             <form action="customer.update{{$customer->id}}" class="form-horizontal" method="post" enctype="multipart/form-data"> @csrf
             <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                 <div class="modal-body font-weight-semibold">
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6">
                                 <label>Company Name</label>
                                 <input type="text" class="form-control" id="customer_name" value="{{ old('customer_name')? old('customer_name') : $customer->customer_name }}" name="customer_name" placeholder="@lang('app.name')">
                                 {!! $errors->has('customer_name')? '<p class="help-block">'.$errors->first('customer_name').'</p>':'' !!}
                             </div>

                             <div class="col-lg-6">
                                 <label>Address</label>
                                 <input type="text" class="form-control" id="Address" value="{{ old('Address')? old('Address') : $customer->address }}" name="address" placeholder="@lang('app.name')">
                             </div>
                         </div>
                     </div>


                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6">
                                 <label>Contact Person</label>
                                 <input type="text" class="form-control" id="contact_person" value="{{ old('contact_person')? old('contact_person') : $customer->contact_person }}" name="contact_person">

                             </div>

                             <div class="col-lg-6">
                                 <label>Contact Number</label>
                                 <input type="text" class="form-control" id="contact_no" value="{{ old('contact_no')? old('contact_no') : $customer->contact_no }}" name="contact_no">
                                 {!! $errors->has('mobile')? '<p class="help-block">'.$errors->first('mobile').'</p>':'' !!}
                             </div>
                         </div>
                     </div>
                     
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-12">
                                 <label>Remarks</label>
                                 <textarea cols="5" rows="5" name="remarks" class="form-control">{{$customer->remarks}}</textarea>

                             </div>
                         </div>
                     </div>

                 </div>

                 <div class="modal-footer">
                     <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn bg-primary">Submit form</button>
                 </div>
             </form>
         </div>
     </div>
 </div>
 <!-- /vertical form modal -->