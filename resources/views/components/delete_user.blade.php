<div id="destroy_user{{ $user->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                <p class="font-weight-semibold font-size-lg">Are you sure you want to delete {{ $user->firstname }} {{ $user->lastname }} ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('destroy_user', $user->id) }}">
                    {{ method_field('DELETE') }}
                    @csrf
                    <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                    <input class="btn btn-primary " type="submit" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>