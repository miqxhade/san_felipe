 <!-- Vertical form modal -->
 <div id="edit_product{{$product->id}}" class="modal fade" tabindex="-1">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header  bg-slate-800">
                 <h5 class="modal-title">Edit {{$product->product_code}}</h5>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>

             <form action="product.update{{$product->id}}" class="form-horizontal" method="post" enctype="multipart/form-data"> @csrf
             <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                 <div class="modal-body font-weight-semibold">
                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-6">
                                 <label>Product Code</label>
                                 <input type="text" class="form-control" id="product_code" value="{{ old('product_code')? old('product_code') : $product->product_code }}" name="product_code" placeholder="@lang('app.name')">
                                 {!! $errors->has('product_code')? '<p class="help-block">'.$errors->first('product_code').'</p>':'' !!}
                             </div>

                             <div class="col-lg-6">
                                 <label>Units</label>
                                 <input type="text" class="form-control" id="units" value="{{ old('units')? old('units') : $product->units }}" name="units">

                             </div>
                         </div>
                     </div>


                     <div class="form-group">
                         <div class="row">
                             <div class="col-lg-12">
                                 <label>description</label>
                                 <textarea cols="5" rows="5" class="form-control"  name="description">{{ $product->description}}</textarea>

                             </div>
                         </div>
                     </div>



                 </div>

                 <div class="modal-footer">
                     <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn bg-primary">Submit form</button>
                 </div>
             </form>
         </div>
     </div>
 </div>
 <!-- /vertical form modal -->