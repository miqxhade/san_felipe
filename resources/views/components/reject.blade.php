<div id="reject{{ $batch_id->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger-400">
                <h4 class="modal-title">Reject Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                <p class="font-weight-semibold font-size-lg">Are you sure you want to reject {{ $batch_id->batch_code }} ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
                <form method="post" action="{{ route('reject.update',$batch_id->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                    <input class="btn btn-primary " type="submit" value="Yes">
                </form>
            </div>
        </div>
    </div>
</div>