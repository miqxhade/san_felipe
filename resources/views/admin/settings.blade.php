@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Manage Accounts')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="#" class="btn bg-slate-800" data-toggle="modal" data-target="#modal_theme_primary">
                    <i class="fa fa-plus"></i> <span>Add User</span>
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Manage Accounts')}}</a>



            </div>
        </div>

    </div>
</div>

<div id="modal_theme_primary" class="modal fade" tabindex="-1">
    <div class="modal-dialog rounded">
        <div class="modal-content">
            <div class="modal-header bg-slate-800">
                <h6 class="modal-title">Add User</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form role="form" method="POST" action="{{ route('users.add') }}" class="driverForm">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>First Name</label>
                                <input type="text" name="first_name" id="first_name" placeholder="ex.. Nathaniel" class="form-control font-size-sm" required>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Last Name</label>
                                <input type="text" name="last_name" id="last_name" placeholder="ex.. Malvar" class="form-control font-size-sm" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Username</label>
                                <input type="text" name="username" id="username" placeholder="Admin" class="form-control font-size-sm" required>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Password</label>
                                <input type="password" name="password" id="password" class="form-control font-size-sm" required>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Account Type</label>
                                <select class="form-control select" name="type" id="type" data-fouc data-placeholder="Select Type" required>
                                    <option></option>
                                    <option value="Admin">Administrator</option>
                                    <option value="Product">Production</option>
                                    <option value="Encoder">Encoder</option>
                                    <option value="Agent">Agent</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="form-group">
                                <label>Location</label>
                                <select class="form-control select" name="location" id="location" data-fouc data-placeholder="Select Location" required>
                                    <option></option>
                                    <option value="Bacoor">Bacoor</option>
                                    <option value="Kawit">Kawit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')

<div class="card">
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-togglable table-striped table-hover datatable-basic">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Account Type</th>
                        <th>Date Created </th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach($users as $user)
                <tr>

                    <td>{{ $user->id }}</td>
                    <td>{{ $user->lastname}}</td>
                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->type }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td class=" list-icons font-size-sm">
                        <a href="#" class="bg-primary py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#edit_user{{$user->id}}"><i class="fa fa-edit"></i> </a>
                        <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item deleteAds" data-toggle="modal" data-target="#destroy_user{{ $user->id }}"><i class="fa fa-trash"></i> </a>
                    </td>
                    @include('components.edit_user')
                    @include('components.delete_user')
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection