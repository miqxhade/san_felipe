@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Customer List')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="#" class="btn bg-slate-800" data-toggle="modal" data-target="#modal_theme_primary">
                    <i class="fa fa-plus"></i> <span>Add Customer</span>
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Customer List')}}</a>



            </div>
        </div>

    </div>
</div>

<div id="modal_theme_primary" class="modal fade" tabindex="-1">
    <div class="modal-dialog rounded">
        <div class="modal-content">
            <div class="modal-header bg-slate-800">
                <h6 class="modal-title">Add Customer</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form role="form" method="POST" action="{{ route('customer.add') }}" class="driverForm">
                {{ csrf_field() }}
                <div class="modal-body">

                <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Company Name</label>
                                <input type="text" name="company_name" id="first_name" placeholder="ex.. Juan" class="form-control font-size-sm" required>
                            </div>
                            <div class="form-group ">
                                <label>Company Address</label>
                                <input type="text" name="address" id="first_name" placeholder="ex.. Juan" class="form-control font-size-sm" required>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Contact Person</label>
                                <input type="text" name="contact_person" id="first_name" placeholder="ex.. Juan" class="form-control font-size-sm" required>
                            </div>
                            <div class="form-group ">
                                <label>Contact Number</label>
                                <input type="text" name="contact_number" id="contact_numbers" value="{{ old('contact_number') }}" placeholder="09263...." minlength="10" class="form-control font-size-sm" required>
                            </div>

                        </div>
                    </div>
                    <div class="row">
        
                    <div class="col-xl-12">
                    <div class="form-group ">
                    <label>Remarks</label>
                        <textarea cols="5" rows="5" name="remarks" class="form-control"></textarea>
                    </div>
                    </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')

<div class="card">
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-togglable table-striped table-hover datatable-basic">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Company Name</th>
                        <th>Address</th>
                        <th>Contact Person</th>
                        <th>Contact Number</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach($customers as $customer)
                <tr>
            
                    <td>{{ $customer->id }}</td>
                    <td>{{ $customer->customer_name}}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ $customer->contact_person }}</td>
                    <td>{{ $customer->contact_no }}</td>
                    <td>{{ $customer->remarks }}</td>
                    <td class=" list-icons font-size-sm">
                        <a href="#" class="bg-primary py-2 px-2 rounded list-icons-item"  data-toggle="modal" data-target="#editcustomer{{$customer->id}}"><i class="fa fa-edit"></i> </a>
                        <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item deleteAds" data-toggle="modal" data-target="#delete_customer{{ $customer->id }}"><i class="fa fa-trash"></i> </a>
                    </td>
                    @include('components.deletecustomer')
                        @include('components.editcustomer')
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection