@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Audit Trail')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Audit Trail')}}</a>



            </div>
        </div>

    </div>
</div>

<!-- /page header -->
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-togglable table-striped table-hover datatable-basic">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>User</th>
                        <th>Activity</th>
                    </tr>
                </thead>

     
                @foreach ($notifications as $notification)
                    <tr>
                        <td>{{$notification->created_at}}</td>
                        <td>{{$notification->data['created_by']}}</td>
                      <!-- Create -->
   @if($notification->type === 'App\Notifications\TaskCompleted')
                        <td> Has added new Product <strong class="text-success">{{$notification->data['product_code']}}</strong> </td>
                        @endif

                        @if($notification->type === 'App\Notifications\CreateCustomer')
                        <td> Has added new Customer <strong class="text-success">{{$notification->data['customer_name']}}.</strong> </td>
                        @endif

                        @if($notification->type === 'App\Notifications\CreateBatch')
                        <td> Has added new Job Order <strong class="text-success">{{$notification->data['batch_code']}}.</strong> </td>
                        @endif

                        @if($notification->type === 'App\Notifications\AcceptedOrder')
                        <td> Has added new Job Order <strong class="text-success">{{$notification->data['batch_code']}}.</strong> </td>
                        @endif

                        @if($notification->type === 'App\Notifications\CompletedOrder')
                        <td> Has Completed Job Order <strong class="text-success">{{$notification->data['batch_code']}}.</strong> </td>
                        @endif

                        <!-- Create -->
                        <!-- Edit -->
                        @if($notification->type === 'App\Notifications\EditCustomer')
                        <td>Has edited Customer <strong class="text-primary">{{$notification->data['customer_name']}}</strong>.</td>
                        @endif

                        @if($notification->type === 'App\Notifications\EditProduct')
                        <td>Has edited Product <strong class="text-primary">{{$notification->data['product_code']}}</strong>.</td>
                        @endif

                        @if($notification->type === 'App\Notifications\EditedOrder')
                        <td>Has edited Job Order <strong class="text-primary">{{$notification->data['batch_code']}}</strong>.</td>
                        @endif


                        <!-- Edit -->

                        <!-- Destroyed -->
                        @if($notification->type === 'App\Notifications\DestryCustomer')
                        <td> Has deleted Customer <strong class="text-danger">{{$notification->data['customer_name']}}</strong>.</td>
                        @endif

                        @if($notification->type === 'App\Notifications\DestroyProduct')
                        <td> Has deleted Customer <strong class="text-danger">{{$notification->data['product_code']}}</strong>.</td>
                        @endif

                        @if($notification->type === 'App\Notifications\rOrdrer')
                        <td> Has Rejected Job Order <strong class="text-danger">{{$notification->data['batch_code']}}</strong>.</td>
                        @endif
                        <!-- Destroyed -->


                    </tr>
                    @endforeach
           

            </table>
        </div>

    </div>
</div>

@endsection