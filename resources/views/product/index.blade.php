@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Product List')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="#" class="btn bg-slate-800" data-toggle="modal" data-target="#modal_theme_primary">
                    <i class="fa fa-plus"></i> <span>Add Product</span>
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Product List')}}</a>



            </div>
        </div>

    </div>
</div>

<div id="modal_theme_primary" class="modal fade" tabindex="-1">
    <div class="modal-dialog rounded">
        <div class="modal-content">
            <div class="modal-header bg-slate-800">
                <h6 class="modal-title">Add Product</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form role="form" method="POST" action="{{ route('product.add') }}" class="driverForm">
                {{ csrf_field() }}
                <div class="modal-body">
                <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Product Code</label>
                                <input type="text" name="product_code" id="product_code" placeholder="ex.. PCOS2" class="form-control font-size-sm" required>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group ">
                                <label>Units</label>
                                <input type="text" name="units" id="units" class="form-control font-size-sm" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label>Description</label>
                                <textarea cols="5" rows="5" class="form-control" name="description"> </textarea>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /page header -->
@endsection

@section('content')

<div class="card">
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-togglable table-striped table-hover datatable-basic">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Product Codes</th>
                        <th>Description</th>
                        <th>Units</th>
                        <th>Created at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach($products as $product)
                <tr>

                    <td>{{ $product->id }}</td>
                    <td>{{ $product->product_code}}</td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->units }}</td>
                    <td>{{ $product->created_at }}</td>
                    <td class=" list-icons font-size-sm">
                        <a href="#" class="bg-primary py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#edit_product{{$product->id}}"><i class="fa fa-edit"></i> </a>
                        <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item deleteAds" data-toggle="modal" data-target="#delete_product{{ $product->id }}"><i class="fa fa-trash"></i> </a>
                    </td>
                    @include('components.delete_product')
                    @include('components.edit_product')

                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection