@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Create Job order')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item">{{__('Job Order')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Job Order Create')}}</a>



            </div>
        </div>

    </div>
</div>

<!-- /page header -->
@endsection

@section('content')


<div class="card">
    <div class="card-body">
        <form role="form" method="POST" action="{{ route('order.add') }}">
            {{ csrf_field() }}
            <input type="hidden" id="new_id" name="new_ids" value="{{$new_id}}">
            <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
            <div class="form-group row">
                <div class="col-md-6">
                    <label>Customer:</label>
                    <select class="form-control  form-control-select2" name="customer" required>
                        <option value="">Select a Company</option>
                        @foreach($customers as $customer)
                        <option value="{{ $customer->customer_name }}" data-type="{{$customer->customer_name}}">{{ $customer->customer_name }}</option>
                        @endforeach
                    </select>

                </div>

                <div class="col-md-6">
                    <label>To:</label>
                    <select class="form-control  form-control-select2" name="user" required>
                        <option value="">Select a User</option>
                        @foreach($users as $user)
                        <option value="{{ $user->id }}" data-category-type="{{$user->firstname}}">{{ $user->firstname }} {{ $user->lastname }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">

                    <label>Date</label>
                    <input type="text" name="date" id="date" value="<?php echo date("Y-m-d"); ?>" class="form-control font-size-sm" readonly>

                </div>
                <div class="col-md-4">
                    <label>Date Needed</label>
                    <input type="date" name="date_needed" id="date_needed" class="form-control font-size-sm" required>
                </div>

                <div class="col-md-4">
                    <label>PO#</label>
                    <input type="text" name="purchase_order" id="purchase_order" class="form-control font-size-sm">

                </div>
            </div>

            <div class="form-group-row mb-1">
                <div class="col-md-12">
                    <label>Additional Notes:</label>
                    <textarea cols="5" rows="5" class="form-control" id="notes" name="notes"></textarea>
                </div>

            </div>

            <div class="table-responsive">
                <table class="table table-togglable table-striped table-hover datatable-basic" id="myTable">
                    <thead>
                        <tr>

                            <th>Product Code</th>
                            <th>Description</th>
                            <th>Unit</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <button type="button" class="btn btn-success btn-add  ml-2">Add Row</button>
                        <tr>

                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="form-group row mr-2">
                <button type="submit" class="btn btn-success  ml-3">Submit</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(".btn-add").click(function() {
        var sa = document.createElement('tr');

        $(sa).html(
            "<td>" +
            "<select class='form-control form-control-select2 samples' name='product_code'>" +
            "<option>Select Product</option>" +
            "@foreach($products as $product)" +
            "<option value=\"{{json_encode($product) }}\" data-type=\"{{$product->product_code}}\">{{ $product->product_code }}</option>" +
            "@endforeach>" +
            "<input class='form-control' type='hidden' id='product_code' name='product_codes[]' />" +
            "<input class='form-control' type='hidden' value=\"{{ $new_id }}\" name='new_id[]' />" +
            "</select>" +
            "</td>" +
            "<td>" +
            "<input class='form-control' type='text' id='descriptions' name='description[]' placeholder='Description' readonly/>" +
            "</td>" +
            "<td>" +
            "<input type='text' class='form-control' id='unitss' name='units[]' placeholder='Units' readonly/>" +
            "</td>" +
            "<td>" +
            "<input type='number' class='form-control'name='quantity[]' placeholder='Quantity' />" +
            "</td>" +
            "<td>" +
            "<button class=' btn btn-danger btn_remove'>-</button>" +
            "</td>");
        $("table tbody").append(sa).change(function() {
            sample();
        });



    });

    $(document).ready(function(){

    $("#myTable").on('click','.btn_remove',function(){
        $(this).parent().parent().remove();
    });
});


    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("date_needed").setAttribute("min", today);
</script>
<script>
    function sample() {
        $("select.samples").change(function() {
            var selectedRecent = $(this).children("option:selected").val();
            console.log($(this).parent().parent().find("product_codes"));


            var samples = JSON.parse(selectedRecent);

            console.log(samples);
            $(this).closest("tr").children().find("#unitss").val(samples.units);
            $(this).closest("tr").children().find("#descriptions").val(samples.description);
            $(this).closest("tr").children().find("#product_code").val(samples.product_code);

        });

    }
</script>


<script>
    @if(session('success'))
    toastr.success('{{ session('
        success ') }}', '{{ trans('
        app.success ') }}', toastr_options);
    @endif
    @if(session('error'))
    toastr.error('{{ session('
        error ') }}', '{{ trans('
        app.success ') }}', toastr_options);
    @endif
</script>
@endsection