@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Job Order List')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Job Order')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Production')}}</a>



            </div>
        </div>

    </div>
</div>

<!-- /page header -->
@endsection



@section('content')
@include('components.complete')
<div class="card">


    <div class="card-body">

        <div class="form-group row">
            <div class="col-md-3">
                <label class="font-weight-semibold">Customer:</label>
                <p>{{ $company->customer_name }} </p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Purchase Order:</label>
                <p>{{ $batch_id->purchase_order }}</p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Date Ordered:</label>
                <p>{{ $batch_id->created_at }}</p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Date Needed:</label>
                <p>{{ $batch_id->date_need }}</p>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-togglable table-striped table-hover datatable-basic table table-xs">
            <thead>
                <tr>
                    <th>Item No.</th>
                    <th>Product Code</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Add Produced Item</th>
                    <th>Produced</th>
                    <th>Update Delivery</th>
                    <th>Delivered</th>
                    <th>Quantity balance</th>

                </tr>
            </thead>
            @foreach($order_id as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->product_code }}</td>
                <td> {{ $order->bond }}</td>
                <td>{{ $order->quantity}}</td>
                <td>
                    <form role="form" method="POST" action="{{ route('produce.add') }}">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" name="item_id" value="{{ $order->id }}">
                            <input type="text" class="form-control" name="produced">

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-success btn-icon"><i class="icon-add text-white"></i></button>
                            </div>
                        </div>
                    </form>

                </td>
                @foreach($sample as $item)
                <td>
                    <form role="form" method="POST" action="{{ route('produce.update') }}">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" name="item_id" value="{{ $order->id }}">
                            <input type="text" class="form-control" name="produced" value="{{$item->produced}}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-danger btn-icon"><i class="icon-subtract text-white"></i></button>
                            </div>
                        </div>
                    </form>                
                </td>
                @endforeach
                <td>
                    <form role="form" method="POST" action="{{ route('deliver.add') }}">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="hidden" class="form-control" name="item_id" value="{{ $order->id }}">
                            <input type="text" class="form-control" name="delivered">

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-success btn-icon"><i class="icon-add text-white"></i></button>
                            </div>
                        </div>
                    </form>

                </td>
                <td>{{ $order->quantity}}</td>
                <td>{{ $order->quantity}}</td>
            </tr>
            @endforeach
        </table>
        
    </div>
    
</div>
@if( Auth::user()->type == 'Encoder' || Auth::user()->type == 'Product')

@else
<div class="col-md-12 text-right">
    <a href="#" data-toggle="modal" data-target="#comeplete{{$batch_code}}" class=" btn btn-success text-center">Complete</a>
</div>
@endif

@endsection