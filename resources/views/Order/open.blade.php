@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Job Order List')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Job Order')}}</a>
                <a href="#" class="breadcrumb-item active">{{ $batch_id->batch_code }}</a>



            </div>
        </div>

    </div>
</div>
@include('components.accept')
@include('components.reject')
<!-- /page header -->
@endsection



@section('content')

<div class="card">

    <div class="card-body p-3">
        <div class="form-group row">
            <div class="col-md-4">
                <label class="font-weight-semibold">Customer:</label>
                <p>{{ $company->customer_name }} </p>


            </div>

            <div class="col-md-4">
                <label class="font-weight-semibold">Date Ordered:</label>
                <p>{{ $batch_id->created_at }}</p>

            </div>

            <div class="col-md-4">
                <label class="font-weight-semibold">Date Needed:</label>
                <p>{{ $batch_id->date_need }}</p>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                <label class="font-weight-semibold">Address:</label>
                <p>{{ $company->address }} </p>


            </div>

            <div class="col-md-4">
                <label class="font-weight-semibold">To:</label>
                <p>{{ $batch_id->batch_code }}</p>

            </div>

            <div class="col-md-4">
                <label class="font-weight-semibold">Po#:</label>
                <p>{{ $batch_id->purchase_order }}</p>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-togglable table-striped table-hover datatable-basic">
            <thead>
                <tr>
                    <th>Item No.</th>
                    <th>Code & Description</th>
                    <th>Quantity</th>

                </tr>
            </thead>
            @foreach($order_id as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->product_code }} {{ $order->bond }}</td>
                <td>{{ $order->quantity}}</td>
            </tr>
            @endforeach


        </table>
    </div>
</div>

@if( Auth::user()->type == 'Admin')

<div class="col-md-12 text-right">
    @if( $batch_id->status == 'approve')
    @if( $batch_id->status == 'approve')
    <p class="bg-success-400 p-2">Approved</p>
    @else
    <p class="bg-danger-400 p-2">Rejected</p>
    @endif

    @else
    <a href="#" class="bg-success py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#accept{{$batch_id->id}}">Accept</a>
    <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#reject{{$batch_id->id}}">Reject</a>

    @endif
</div>
@else
<!-- <div class="col-md-12 text-right">
    @if( $batch_id->status == 'approve')
    @if( $batch_id->status == 'approve')
    <p class="bg-success-400 p-2">Approved</p>
    @else
    <p class="bg-danger-400 p-2">Rejected</p>
    @endif

    @else
    <a href="#" class="bg-success py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#accept{{$batch_id->id}}">Accept</a>
    <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#reject{{$batch_id->id}}">Reject</a>

    @endif
</div> -->
@endif







@endsection