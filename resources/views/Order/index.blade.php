@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Job Order List')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>



        <div class="header-elements d-none">

            @if( Auth::user()->type == 'Encoder' || Auth::user()->type == 'Admin' )
            <div class="d-flex justify-content-center">
                <a href="{{ route('order.create') }}" class="btn ticon-lef btn-primary font-weight-semibold">
                    <i class="fa fa-plus"></i> {{ __('Create') }}
                </a>
            </div>
            @else

            @endif
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Job Order')}}</a>



            </div>
        </div>

    </div>
</div>
<!-- /page header -->
@endsection



@section('content')

<div class="card">


    <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
        <li class="nav-item"><a href="#highlighted-justified-tab1" class="nav-link active" data-toggle="tab">Pending</a></li>
        <li class="nav-item"><a href="#highlighted-justified-tab2" class="nav-link" data-toggle="tab">On Production</a></li>
        <li class="nav-item"><a href="#highlighted-justified-tab3" class="nav-link" data-toggle="tab">Completed</a></li>
    </ul>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="highlighted-justified-tab1">
                <div class="table-responsive">
                    <table class="table table-togglable table-striped table-hover datatable-basic">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>JO Number</th>
                                <th>Customer</th>
                                <th>Date Ordered</th>
                                <th>Date Needed</th>
                                @if( Auth::user()->type == 'Encoder' || Auth::user()->type == 'Admin' )
                                <th style="width: 120px;">Action</th>
                                @else

                                @endif
                            </tr>
                        </thead>
                        @foreach($pendings as $pending)
                        @if($pending->date_need == $now|| $pending->date_need == $tom|| $pending->date_need == $future)
                        <tr style="color:red">
                            <td>{{ $pending->id }}</td>
                            <td>{{ $pending->batch_code }}</td>
                            <td>{{ $pending->order_by}}</td>
                            <td>{!! date('Y-m-d', strtotime($pending->created_at)) !!}</td>
                            <td>{{ $pending->date_need }}</td>
                            @if( Auth::user()->type == 'Encoder')
                            <td class=" list-icons font-size-sm">
                                <a href="{{ route('order.update', $pending->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                            </td>

                            @else
                            @if( Auth::user()->type == 'Encoder' || Auth::user()->type == 'Admin' )
                            <td class=" list-icons font-size-sm" style="width: 120px;">


                                <a href="{{ route('order.edit', $pending->id) }}" class="bg-primary py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                                <a href="{{ route('order.update', $pending->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                                <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#pending{{ $pending->id }}"><i class="fa fa-trash"></i> </a>

                            </td>
                            @else

                            @endif
                            @endif
                   
                        </tr>
                        @else
                        <tr>
                            <td>{{ $pending->id }}</td>
                            <td>{{ $pending->batch_code }}</td>
                            <td>{{ $pending->order_by}}</td>
                            <td>{!! date('Y-m-d', strtotime($pending->created_at)) !!}</td>
                            <td>{{ $pending->date_need }}</td>
                            @if( Auth::user()->type == 'Encoder')
                            <td class=" list-icons font-size-sm">
                                <a href="{{ route('order.update', $pending->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                            </td>

                            @else
                            @if( Auth::user()->type == 'Encoder' || Auth::user()->type == 'Admin' )
                            <td class=" list-icons font-size-sm" style="width: 120px;">


                                <a href="{{ route('order.edit', $pending->id) }}" class="bg-primary py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                                <a href="{{ route('order.update', $pending->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                                <a href="#" class="bg-danger py-2 px-2 rounded list-icons-item" data-toggle="modal" data-target="#pending{{ $pending->id }}"><i class="fa fa-trash"></i> </a>

                            </td>
                            @else

                            @endif
                            @endif
                        </tr>
                        @endif


                        @include('components.deleteList')
                        @include('components.order_view')
                        @include('components.order_edit')
                        @endforeach

                    </table>
                </div>
            </div>

            <div class="tab-pane fade" id="highlighted-justified-tab2">
                <div class="table-responsive">
                    <table class="table table-togglable table-striped table-hover datatable-basic">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Customer</th>
                                <th>Date Ordered</th>
                                <th>Date {{$now}}</th>
                                <th style="width: 120px;">Action</th>
                            </tr>
                        </thead>
                        @foreach($onproduction as $production)


                        @if($production->date_need == $now|| $production->date_need == $tom || $production->date_need == $future)

                    
                        <tr style="background-color:red">
                            <td>{{ $production->id }}</td>
                            <td>{{ $production->order_by}}</td>
                            <td>{!! date('Y-m-d', strtotime($production->created_at)) !!}</td>
                            <td>{{ $production->date_need }}</td>
                            <td class=" list-icons font-size-sm" style="width: 120px;">
                                <a href="{{ route('production.update', $production->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                            </td>

                        </tr>
                        @else
                        <tr>
                            <td>{{ $production->id }}</td>
                            <td>{{ $production->order_by}}</td>
                            <td>{!! date('Y-m-d', strtotime($production->created_at)) !!}</td>
                            <td>{{ $production->date_need }}</td>
                            <td class=" list-icons font-size-sm" style="width: 120px;">
                                <a href="{{ route('production.update', $production->id) }}" class="bg-success py-2 px-2 rounded list-icons-item" data-popup="tooltip" title="Open"><i class="fa fa-eye"></i> </a>
                            </td>

                        </tr>

                        @endif
                        @endforeach
                    </table>
                </div>
            </div>

            <div class="tab-pane fade" id="highlighted-justified-tab3">
                <div class="table-responsive">
                    <table class="table table-togglable table-striped table-hover datatable-basic">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>JO Number</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Date Ordered</th>
                                <th>Date Needed</th>
                            </tr>
                        </thead>
                        @foreach($completed as $complete)
                        <tr>

                            <td>{{ $complete->id }}</td>
                            <td>{{ $complete->batch_code }}</td>
                            <td>{{ $complete->order_by}}</td>

                            @if($complete->status != 'completed')
                            <td class="text-danger">{{ $complete->status}}</td>
                            @else
                            <td class="text-success">{{ $complete->status}}</td>
                            @endif
                            <td>{!! date('Y-m-d', strtotime($pending->created_at)) !!}</td>
                            <td>{{ $complete->date_need }}</td>

                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection