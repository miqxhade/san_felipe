@extends('layouts.master')

@section('page_header')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Job Order Edit')}}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> {{__('Dashboard')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Job Order')}}</a>
                <a href="#" class="breadcrumb-item active">{{__('Edit')}}</a>



            </div>
        </div>

    </div>
</div>

<!-- /page header -->
@endsection



@section('content')
<div class="card">


    <div class="card-body">


        <div class="form-group row">
            <div class="col-md-3">
                <label class="font-weight-semibold">Customer:</label>
                <p>{{ $company->customer_name }} </p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Purchase Order:</label>
                <p>{{ $batch_id->purchase_order }}</p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Date Ordered:</label>
                <p>{{ $batch_id->created_at }}</p>
            </div>

            <div class="col-md-3">
                <label class="font-weight-semibold">Date Needed:</label>
                <p>{{ $batch_id->date_need }}</p>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-togglable table-striped table-hover datatable-basic table table-xs">
            <thead>
                <tr>
                    <th>Item No.</th>
                    <th>Product Code</th>
                    <th>Product Description</th>
                    <th>Quantity</th>


                </tr>
            </thead>
            @foreach($order_id as $order)
            <tr>



                <td>{{ $order->id }}</td>
                <td>{{ $order->product_code }}</td>
                <td> {{ $order->bond }}</td>
                <form action="order.update{{$order->id}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}" name="created_by">
                    <td>
                        <div class="input-group">
                            <input type="number" class="form-control" name="quantity" value="{{ old('quantity')? old('quantity') : $order->quantity }}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-success btn-icon"><i class="icon-add text-white"></i></button>
                            </div>
                        </div>
                    </td>
                </form>
            </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection