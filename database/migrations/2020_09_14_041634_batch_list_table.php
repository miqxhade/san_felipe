<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BatchListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_list', function (Blueprint $table) {
            $table->id();
            $table->string('batch_code')->nullable();
            $table->string('date_need')->nullable();
            $table->string('order_by')->nullable();
            $table->string('user_id')->nullable();
            $table->string('isViewed')->nullable();
            $table->string('ping')->nullable();
            $table->string('status')->nullable();
            $table->string('note')->nullable();
            $table->string('purchase_order')->nullable();
            $table->string('accepted_by')->nullable();
            $table->string('rejected_by')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_list');
    }
}
