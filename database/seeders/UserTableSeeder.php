<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'sysadmin',
            'lastname' => 'sysadmin',
            'email' => 'system@admin.com',
            'username' => 'sysadmin',
            'password' => Hash::make('12345678'),
            'type' => 'Admin',
            'location' => 'Bacoor',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'firstname' => 'fealene',
            'lastname' => 'sabangan',
            'email' => 'production@admin.com',
            'username' => 'production',
             'password' => Hash::make('12345678'),
            'type' => 'Product',
            'location' => 'Bacoor',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'firstname' => 'nath',
            'lastname' => 'malvar',
            'email' => 'agent@admin.com',
            'username' => 'Agent',
             'password' => Hash::make('12345678'),
            'type' => 'Agent',
            'location' => 'Bacoor',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'firstname' => 'Marco',
            'lastname' => 'James',
            'email' => 'encoder@admin.com',
            'username' => 'Encoder',
             'password' => Hash::make('12345678'),
            'type' => 'Encoder',
            'location' => 'Bacoor',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
